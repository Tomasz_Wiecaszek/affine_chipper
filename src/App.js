import { React, useState } from "react";
import { v4 as uuidv4 } from "uuid";
import "./style.css";
const App = () => {
  //NUMBER OF LETTERS
  const alpha = 26;
  
  const initialState = {"state":"initialState"};
  //STATE WITH CURRENT OBJECT JSON
  let [currentFile, setCurrentFile] = useState(initialState);
  let [currentFileName, setCurrentFileName] = useState("");
  let [showTip, setShowTip] = useState(false);
// REGEX TO CHECK TXT / CRYPTOGRAM  
  const reg = /^[a-zA-Z]+$/gi;
// ALL FUNCTIONS
  const gcd = (a, b) => {
    var q;
    while (b !== 0) {
      q = a;
      a = b;
      b = q % b;
    }
    return a;
  };

  const checkFile = (file) => {
    if (
      (file.operation === "decrypt" || file.operation === "encrypt") &&
      gcd(parseInt(file.a), alpha) === 1 &&
      parseInt(file.b) <= alpha &&
      (reg.test(file.plainText) === true || reg.test(file.cryptogram) === true)
    )
      return true;
    else return false;
  };

  const encrypt = (file) => {
    const a = parseInt(file.a);
    const b = parseInt(file.b);

    let encrString = "";
    for (let letter in file.plainText.toLowerCase()) {
      let letterCode = file.plainText[letter].toLowerCase().charCodeAt(0) - 97;
      encrString += String.fromCharCode(((letterCode * a + b) % alpha) + 97);
    }
    return {
      a: file.a,
      b: file.b,
      operation: file.operation,
      plainText: file.plainText,
      cryptogram: encrString,
    };
  };

  const decrypt = (file) => {
    const a = parseInt(file.a);
    const b = parseInt(file.b);
    let decrString = "";

    //Find invert a in ring Z26
    for (let j = 1; j < alpha; j++) {
      if ((a * j) % alpha === 1) var invert = j;
    }

    for (let i in file.cryptogram) {
      let finalDecrLetter;

      if (
        ((invert * (file.cryptogram[i].toLowerCase().charCodeAt(0) - 97 - b)) %
          alpha) +
          97 >
        96
      ) {
        finalDecrLetter = String.fromCharCode(
          ((invert *
            (file.cryptogram[i].toLowerCase().charCodeAt(0) - 97 - b)) %
            alpha) +
            97
        );
      } else {
        finalDecrLetter = String.fromCharCode(
          ((invert *
            (file.cryptogram[i].toLowerCase().charCodeAt(0) - 97 - b)) %
            alpha) +
            97 +
            26
        );
      }

      decrString += finalDecrLetter;
    }
    return {
      a: file.a,
      b: file.b,
      operation: file.operation,
      cryptogram: file.cryptogram,
      plainText: decrString,
    };
  };

  const main = (file) => {
    if (file.operation === "encrypt" && checkFile(file) === true) {
      return encrypt(file);
    } else if (file.operation === "decrypt" && checkFile(file) === true) {
      return decrypt(file);
    } else {
      return "Error in JSON source file, check your parameters a,b and operation and try again";
    }
  };

  let uploadFile = (event) => {
    var file = event.target.files[0];
    setCurrentFileName(file.name);

    var reader = new FileReader();
    reader.onload = function (event) {
      console.log(event.target.result);
    };
    reader.onloadend = function (event) {
      setCurrentFile(JSON.parse(event.target.result));
    };
    reader.readAsText(file);
  };
// USER INTERFACE
  return (
    <div className="main_wrapper">
      <div className="App">
        <h1>Affine cipher APP</h1>

        <label htmlFor="file">
          <div className="app_select">LOAD FILE</div>
        </label>
        <input id="file" type="file" accept=".json" onChange={uploadFile} />
        <hr />
        <button
          onClick={() => {
            setShowTip(!showTip);
            
          }}
        >
          Info
        </button>
        {checkFile(currentFile) ? (
          <div>
            <table>
              <tbody>
                <tr>
                  <td>Operation type: </td>
                  <td className="app_table-content">{currentFile.operation}</td>
                </tr>
                <tr>
                  <td>Key A: </td>
                  <td className="app_table-content">{currentFile.a}</td>
                </tr>
                <tr>
                  <td>Key B: </td>
                  <td className="app_table-content">{currentFile.b}</td>
                </tr>
                <tr>
                  <td>
                    {currentFile.operation === "encrypt" ? "Text" : "Cryptogram"}
                  </td>
                  <td className="app_table-content">
                    {currentFile.operation === "encrypt"
                      ? currentFile.plainText
                      : currentFile.cryptogram}
                  </td>
                </tr>
              </tbody>
            </table>
            <a
              href={`data:text/json;charset=utf-8,${encodeURIComponent(
                JSON.stringify(main(currentFile))
              )}`}
              download={
                currentFileName.split(".")[0] + "___" + uuidv4() + ".json"
              }
            >
              <button>SAVE RESULT IN JSON FILE</button>
            </a>
          </div>
        ) : currentFile.state==="initialState"? 
          <p className="app_noSelected">No selected file</p>
         : 
          <p className="app_noSelected">
            Error in JSON source file, check your parameters a,b and operation
            and try again
          </p>
        }
      </div>

      {showTip ? (
        <div className="tip">
          1. This Application help you to encrypt or to decrypt text by affine
          cipher
          <br />
          2. Your text must contain only letters from a-z alphabet (26 chars)
          <br />
          3. Your file must have special format (more in readme.txt)
          <br />
          4. Click Load File to open file
          <br />
          5. Click Save Result in JSON file to save result on your computer
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default App;
